from pytube import YouTube as YT
import moviepy.editor as mp
import sys
import os

def get_best_video(youtube = None):
	opt_resolution = []
	resolutions = ['1080p', '720p', '480p', '360p', '240p']
	ff_dict = {'mp4': 0, 'flv': 1, 'avi': 2, 'mpg': 3, 'wmv': 4, 'webm': 5, 'mov': 6}
	ff_list = ['mp4', 'flv', 'avi', 'mpg', 'wmv', 'webm', 'mov']
	curr_res = None
	curr_form = None
	count = 0
	while len(opt_resolution) == 0:
		try:
			curr_res = resolutions[count]
			opt_resolution = youtube.filter(resolution = curr_res)
			count+=1
		except IndexError:
			# catches index out of bounds exception in case of no good resolutions
			print 'ERROR: Unable to find video'
			quit(-1)
	mini, mini_ind = 0, 0
	assert len(opt_resolution) > 0
	for i in range(0, len(opt_resolution)):
		if mini < opt_resolution[i].extension:
			mini, mini_ind = int(ff_dict[opt_resolution[i].extension]), i
	curr_form = ff_list[mini]
	return curr_res, curr_form

def get_audio(fn, keep_vid):
	print 'Converting to mp3'
	mus = mp.VideoFileClip(fn + '.' + form)
	mus.audio.write_audiofile(fn + '.mp3')
	if keep_vid:
		print 'Cleaning up'
		os.remove(fn + '.' + form)

def cleanup_url(url):
	if url[:8] != 'https://':
		url = 'https://' + url
	if url[8:23] != 'www.youtube.com':
		print url[8:23]
		print 'ERROR: Not a youtube url'
		quit(-1)
	return url

def main():
	global res, form
	get_aud = False if len(sys.argv) < 2 else bool(sys.argv[1])
	if get_aud:
		keep_vid = False if len(sys.argv) < 3 else sys.argv[2]
	url = cleanup_url(raw_input('Input the youtube url: '))
	yt = YT(url)
	slash = '/' if sys.platform != 'windows' else '\\'
	res, form = get_best_video(yt)
	vid = yt.get(form, res)
	print 'Downloading Video'
	print yt.filename
	print '%s - %s' % (res, form)
	vid.download('tmp' + slash)
	fn = 'tmp' + slash + yt.filename
	if get_aud:
		get_audio(fn, keep_vid)

if __name__ == '__main__':
	main()
